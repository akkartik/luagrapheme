package = "luagrapheme"
version = "0.0-1"
source = {
   url = "https://codeberg.org/akkartik/luagrapheme",
}
description = {
   summary = "Lua bindings for Laslo Hunhold's libgrapheme",
   detailed = [[
      libgrapheme is an extremely simple freestanding C99 library providing
      utilities for properly handling strings according to the latest Unicode
      standard 15.0.0. It offers fully Unicode compliant:

      * grapheme cluster (i.e. user-perceived character) segmentation
      * word segmentation
      * sentence segmentation
      * detection of permissible line break opportunities
      * case detection (lower-, upper- and title-case)
      * case conversion (to lower-, upper- and title-case)
   ]],
   homepage = "https://libs.suckless.org/libgrapheme",
   license = "MIT/X11",
}
dependencies = {
   "lua >= 5.1, < 5.4",
}
external_dependencies = {
   LIBGRAPHEME = {
      header = "grapheme.h"
   }
}
build = {
   type = "builtin",
   modules = {
    luagrapheme = {
      sources = {'luagrapheme.c'},
      libraries = {'grapheme'},
      incdirs = {"$(LIBGRAPHEME_INCDIR)"},
      libdirs = {"$(LIBGRAPHEME_LIBDIR)"}
    }
   },
}
