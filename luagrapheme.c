#include <stdint.h>
#include <grapheme.h>
#include <stdio.h>

#include <lua.h>
#include <lauxlib.h>

/* Lua signature:
     bool, state = function (codepoint, codepoint, state)
   state is an int or nil */
static int luagrapheme_is_break(lua_State* L) {
  /* C signature in libgrapheme:
   *  bool grapheme_is_character_break(uint_least32_t cp1, uint_least32_t cp2, uint_least16_t *state) */
  uint_least32_t codepoint1 = luaL_checkinteger(L, 1);
  uint_least32_t codepoint2 = luaL_checkinteger(L, 2);
  uint_least16_t grapheme_state = 0;
  if (!lua_isnil(L, 3))
    grapheme_state = luaL_checkinteger(L, 3);
  int result = grapheme_is_character_break(codepoint1, codepoint2, &grapheme_state);
  lua_pushboolean(L, result);
  lua_pushinteger(L, grapheme_state);
  return 2;
}

int luaopen_luagrapheme(lua_State* L) {
  lua_newtable(L);
  const luaL_Reg nativeFuncLib [] = {
    {"is_break", luagrapheme_is_break},
    {NULL, NULL}
  };
  luaL_setfuncs(L, nativeFuncLib, 0);
  return 1;
}
