# Lua bindings for libgrapheme

[libgrapheme](https://libs.suckless.org/libgrapheme) is an extremely simple
freestanding C99 library providing utilities for properly handling strings
according to the latest Unicode standard 15.0.0. It offers fully Unicode
compliant:

  * grapheme cluster (i.e. user-perceived character) segmentation
  * word segmentation
  * sentence segmentation
  * detection of permissible line break opportunities
  * case detection (lower-, upper- and title-case)
  * case conversion (to lower-, upper- and title-case)

This repo hopes to provide bindings for libgrapheme. Currently extremely
incomplete. Only tested on Linux.

## Installation

First install libgrapheme somewhere from https://libs.suckless.org/libgrapheme.

Either compile it directly:
```
gcc -I/usr/include/lua5.3 -fPIC -c luagrapheme.c
gcc -shared -o luagrapheme.so luagrapheme.o -lgrapheme
lua grapheme_test.lua
```

(Include -I and -L if libgrapheme is somewhere non-standard.)

Or using [Luarocks](https://luarocks.org):

```
luarocks --local make
lua grapheme_test.lua
```

If you installed libgrapheme somewhere nonstandard, tell luarocks where to
find it. For example:

```
luarocks --local make LIBGRAPHEME_DIR=$HOME/.local
```

The rockspec approach lets you move `grapheme_test.lua` to any other directory
and run it from there.

## Interface

* `is_break`: return whether there's a grapheme-cluster boundary between two
  codepoints.

  Arguments:
    * codepoint 1
    * codepoint 2
    * state (integer)

  Returns:
    * boolean
    * new state

  [libgrapheme page](https://libs.suckless.org/libgrapheme/man/grapheme_is_character_break.3)

## Credits

Many thanks to libgrapheme and to https://kevinboone.me/lualib.html
