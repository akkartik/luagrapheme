utf8 = require('utf8')
grapheme = require('luagrapheme')

s = 'Tëst 👨‍👩‍👦 🇺🇸 नी நி!'
print(s)
print('length in bytes:', #s)

num_codepoints = utf8.len(s)
print('length in codepoints:', num_codepoints)

num_graphemes = 0
previous_codepoint = nil
grapheme_state = nil
for pos, current_codepoint in utf8.codes(s) do
  if previous_codepoint == nil then
    num_graphemes = num_graphemes + 1
  else
    local is_break
    is_break, grapheme_state = grapheme.is_break(previous_codepoint, current_codepoint, grapheme_state)
    if is_break then
      num_graphemes = num_graphemes + 1
    end
  end
  previous_codepoint = current_codepoint
end
print('length in graphemes:', num_graphemes)
